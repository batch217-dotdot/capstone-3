import { Card, Button, Modal, Container, Row, Col} from 'react-bootstrap';
import { Link } from "react-router-dom";
import {useState, useEffect, React, useContext} from 'react'
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function ProductCard({productProp}) {


    const { user } = useContext(UserContext);

	const { _id, name, description, price } = productProp;

    const [viewDetails, setViewDetails] = useState(false)

    const [currProductId, setCurrProductId] = useState('')
    const [currName, setCurrName] = useState('')
    const [currPrice, setCurrPrice] = useState('')
    const [currDescription, setCurrDescription] = useState('')
    const [currQuantity, setCurrQuantity] = useState(0)

    const [isActive, setIsActive] = useState(false);
    const [isZero, setIsZero] = useState(true);

    const closeModal = () => {
        setViewDetails(false)
        setCurrQuantity(0)
    }
    const viewModal = (id) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
        .then(res => res.json())
        .then(data => {
            setCurrProductId(data._id)
            setCurrName(data.name)
            setCurrDescription(data.description)
            setCurrPrice(data.price)
            setViewDetails(true)
        })
    }

    const orderProduct = () => {

        fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                userId: user.id,
                products: {
                    productId: currProductId,
                    quantity: currQuantity
                },
                totalAmount: currQuantity*currPrice
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "ORDER SUCCESS!",
                    icon: "success",
                    text: `"The new order was added to the order list.`,
                });
                closeModal()
                setCurrQuantity(0);
            }
            else {
                Swal.fire({
                    title: "ORDER UNSSUCCESSFUL!",
                    icon: "error",
                    text: `The system is experiencing trouble at the moment. Please try again later.`,
                });
                closeModal()
                setCurrQuantity(0);
            }
        })
    }
        
    useEffect(() => {

        if(currQuantity <= 0){
            setIsActive(false)
            setIsZero(true)
        }else{
            setIsActive(true);
            setIsZero(false)
        }

    });


    return (
        <>
            <Card className="my-3">
                <Card.Body>
                    <Row>
                        <Col xs="4">
                            <img
                                src="https://source.unsplash.com/random/?electronic?&"
                                alt="example" className="product-img"
                              />
                        </Col>
                        <Col xs="8">
                            <Card.Title>
                                {name}
                            </Card.Title>
                            <Card.Subtitle>
                                Description:
                            </Card.Subtitle>
                            <Card.Text>
                                {description}
                            </Card.Text>
                            <Card.Subtitle>
                                Price:
                            </Card.Subtitle>
                            <Card.Text>
                                Php {price}
                            </Card.Text>
                            <Button variant="primary" onClick={() => viewModal(_id)}>Details</Button>
                        </Col>
                    </Row>
                </Card.Body>

            </Card>

            <Modal show={viewDetails} onHide={closeModal}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                  <Modal.Title> Product Details</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Row className="justify-content-center">
                            <Col>
                                <img
            src="https://source.unsplash.com/random/?electronic?&"
            alt="example"
          />
                            </Col>
                            <Col>
                                <div>
                                    <h5>Product Name: <span className="result-data">{currName}</span></h5>
                                    <h5>Price: <span className="result-data">{currPrice}</span></h5>
                                    <h5>Description: <span className="result-data">{currDescription}</span></h5>
                                    <h5>Quantity : 
                                        {
                                            isZero
                                            ?
                                            <Button className="counter" variant="secondary" onClick={()=>setCurrQuantity(currQuantity-1)} disabled>-</Button>
                                            :
                                            <Button className="counter" variant="secondary" onClick={()=>setCurrQuantity(currQuantity-1)}>-</Button>
                                        }
                                        <span className="result-data">{currQuantity}</span>
                                        <Button className="counter" variant="secondary" onClick={()=>setCurrQuantity(currQuantity+1)}>+</Button>
                                    </h5>
                                    <h5>Total Amount: <span className="result-data">{currQuantity*currPrice}</span></h5>
                                    
                                </div>
                            </Col>
                        </Row>
                    </Container>

                </Modal.Body>
                <Modal.Footer className="justify-content-center">
                {
                    isActive && !user.isAdmin && user.id !== null
                    ?
                    <Button variant="primary" onClick={orderProduct}>
                        Order Product
                    </Button>
                    :
                    <Button variant="primary" disabled>
                        Order Product
                    </Button>

                }
                </Modal.Footer>
              </Modal>
        </>

    )
}