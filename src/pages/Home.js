import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home(){
	const data = {
		title: "Zalada Online Shop",
		content: "Shop for everyone, everywhere!",
		destination: "/products",
		label: "Order now!"
	}

	return(
		<>
			<Banner bannerProp={data}/>
			<Highlights />
		</>
	)
}